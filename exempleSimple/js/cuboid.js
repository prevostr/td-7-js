const yellowRectangle = document.getElementById("yellowRectangle");

import {applyAndRegister, reactive, startReactiveDom} from "./reactiveMinified.js";

let cuboid  = reactive( {
  length: 2, 
  width : 3,
  height: 4,
    volume: function (){
        return this.length * this.width * this.height;
    },
  incrLength: function (){
    this.length++;
  },
  decrLength: function (){
    this.length--;
  },
  incrWidth: function (){
    this.width++;

  },
  decrWidth: function (){
    this.width--;

  },
  incrHeight: function (){
    this.height++;

  },
  decrHeight: function (){
    this.height--;
  },
  monStyle: function () {
        return {
          width: (10 * this.width) + 'px',
          height: (10 * this.height) + 'px'
        };
   },
}, "cuboidHTML");


startReactiveDom();

