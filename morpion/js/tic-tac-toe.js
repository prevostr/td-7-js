import {applyAndRegister, reactive, startReactiveDom} from "./reactiveMinified.js";

let previousState = [{},{},{},{},{},{},{},{},{}];

let game = reactive({board:Array(9).fill(""), nextPlayer: "O", winner:undefined, roundNumber:1}, "game");

game.getboard = function(i){
  return this.board[i];
}

game.isWinner = function(P){
  const winLines = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]];
  for(let line of winLines){
    if(game.board[line[0]] === P && game.board[line[1]] === P && game.board[line[2]] === P)
      return true;
  }
  return false;
}


game.restart = function(){
    this.board = Array(9).fill("");
    this.nextPlayer = "O";
    this.winner = undefined;
    this.roundNumber = 1;
}

game.play = function (i) {
    if (this.board[i] !== "" || this.winner !== undefined) return;
  let newBoard = [...this.board];
  newBoard[i] = this.nextPlayer;
  this.roundNumber++;
  this.board = newBoard;
  this.nextPlayer = this.nextPlayer === "O" ? "X" : "O";

  Object.assign(previousState[this.roundNumber], this);

}

game.setWinner = function(){
  if(this.isWinner("O"))
    this.winner = "O";
  else if(this.isWinner("X"))
    this.winner = "X";
  else if(this.roundNumber === 10)
    this.winner = "draw";
}

game.round = function(i){
  console.log(`Bouton round${i} utilisé.`);
    Object.assign(this, previousState[i]);
}
game.backRoundButtons = function (){
  let o = "";
  for (let s =0; s<game.roundNumber; s++){
    o += `<button data-onclick="game.round(${s})">${s+1}</button>`;
  }
  return o;
}

applyAndRegister(() =>{game.setWinner()});
startReactiveDom();
