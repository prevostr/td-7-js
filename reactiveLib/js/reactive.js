let objectByName = new Map();

function applyAndRegister(effect){
  effect();
}

function reactive(passiveObject, name){
  objectByName.set(name, passiveObject);
  return passiveObject;
}

function startReactiveDom(){
  for (let elementClickable of document.querySelectorAll("[data-onclick]")){
    const [nomObjet, methode, argument] = elementClickable.dataset.onclick.split(/[.()]+/);

    elementClickable.addEventListener('click', (event) => {
      const objet = objectByName.get(nomObjet);
      objet[methode](argument);
    })
  }
  /* for (let elementTEXT of document.querySelectorAll("[data-textfun]")){
    const [nomObjet, methode, argument] = elementTEXT.dataset.textfun.split(/[.()]+/);
  elementTEXT.textContent= objectByName.get(nomObjet)[methode](argument);

  } */
  for (let rel of document.querySelectorAll("[data-textfun]")){
    const [obj, fun, arg] = rel.dataset.textfun.split(/[.()]+/);
    applyAndRegister(()=>{rel.textContent = objectByName.get(obj)[fun](arg)});
  }
  for (let rel of document.querySelectorAll("[data-textvar]")){
    const [obj, prop] = rel.dataset.textvar.split('.');
    applyAndRegister(()=>{rel.textContent = objectByName.get(obj)[prop]});
  }

}
export {applyAndRegister, reactive, startReactiveDom};
